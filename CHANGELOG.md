# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

# [Version 0.3.2]
### Changed
- Bump deps and version

# [Version 0.3.0]
### Changed
- Add many more systemd interfaces (Maarten de Vries)

# [Version 0.2.1]
### Changed
- Fixed some typos in systemd sub-state string representations.

# [Version 0.2.0]
### Changed
- Upgrade zbus to 4.0.1

# [Version 0.1.1]
### Changed
- Add gitlab pipeline
- Add cargo deny
- Add cargo cranky
