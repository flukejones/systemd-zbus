//! Tests are incomplete as the majority of them will require root access

// use futures_lite::future;

use crate::{LoadState, ManagerProxyBlocking, Mode, MountProxyBlocking, TimerProxyBlocking};

#[test]
fn reload_or_restart_unit() {
    let conn = zbus::blocking::Connection::system().unwrap();
    let proxy = ManagerProxyBlocking::new(&conn).unwrap();

    assert!(proxy
        .reload_or_restart_unit("nvidia-powerd.service", Mode::Fail)
        .is_ok());
}

// #[test]
// fn enqueue_unit_job() {
//     let conn = zbus::blocking::Connection::system().unwrap();
//     let proxy = ManagerProxyBlocking::new(&conn).unwrap();

//     let res = proxy.enqueue_unit_job("nvidia-powerd.service", "", Mode::Replace).unwrap();
// }

#[test]
fn list_unit() {
    let conn = zbus::blocking::Connection::system().unwrap();
    let proxy = ManagerProxyBlocking::new(&conn).unwrap();

    proxy.list_units().unwrap();
    proxy.list_units().unwrap();
    assert!(proxy.list_units().is_ok());
    assert!(proxy
        .list_units_by_names(&["nvidia-powerd.service", "asusd.service"])
        .is_ok());
    assert!(proxy
        .list_units_by_patterns(&[LoadState::Loaded], &["nvidia*"])
        .is_ok());
    assert!(proxy.list_units_filtered(&[LoadState::Loaded]).is_ok());
}

#[test]
#[ignore = "Requires root"]
fn job() {
    let conn = zbus::blocking::Connection::system().unwrap();
    let proxy = ManagerProxyBlocking::new(&conn).unwrap();

    proxy.set_default_target("graphical.target", true).unwrap();
    proxy
        .enable_unit_files(&["nvidia-powerd.service"], true, true)
        .unwrap();
    assert!(proxy.get_job(0).is_ok());
    assert!(proxy
        .list_units_by_names(&["nvidia-powerd.service", "asusd.service"])
        .is_ok());
    assert!(proxy
        .list_units_by_patterns(&[LoadState::Loaded], &["nvidia*"])
        .is_ok());
    assert!(proxy.list_units_filtered(&[LoadState::Loaded]).is_ok());
}

// timer /org/freedesktop/systemd1/unit/run_2duser_2d1000_2emount
// mount /org/freedesktop/systemd1/unit/home_2emount

#[test]
fn mounts() {
    let conn = zbus::blocking::Connection::system().unwrap();
    let proxy =
        MountProxyBlocking::new(&conn, "/org/freedesktop/systemd1/unit/home_2emount").unwrap();

    let mode = proxy.directory_mode().unwrap();
    assert_eq!(mode, 493);
}

#[test]
fn timers() {
    let conn = zbus::blocking::Connection::session().unwrap();
    let proxy = TimerProxyBlocking::new(
        &conn,
        "/org/freedesktop/systemd1/unit/grub_2dboot_2dsuccess_2etimer",
    )
    .unwrap();

    let calendar = proxy.timers_calendar().unwrap();
    assert_eq!(calendar, Vec::default());

    let timers = proxy.timers_monotonic().unwrap();
    dbg!(&timers);
    assert_eq!(
        timers,
        vec![crate::TimerMonotonic {
            base: "OnActiveUSec".to_string(),
            offset_usec: 120000000,
            next_elapse_realtime_usec: 151258638
        }]
    );

    let accur = proxy.accuracy_usec().unwrap();
    dbg!(&accur);
    assert_eq!(accur, 60000000);
}

// #[test]
// fn properties_async() {
//     future::block_on(async {
//         let conn = zbus::Connection::system().await.unwrap();
//     });
// }
